<?php

class FeedsWesabeParser extends FeedsParser {

  /**
   * Parses a raw string and returns a Feed object from it.
   */
  public function parse(FeedsFetcherResult $fetcherResult, FeedsSource $source) {
    if ($fetcherResult->type == 'text/filepath') {
      $string = file_get_contents($fetcherResult->value);
    }
    else {
      $string = $fetcherResult->value;
    }

    $result_rows = array();
    $doc = new DOMDocument();
    $doc->loadXML($string);
    
    $currency = self::firstTagValue($doc,'currency');
    
    $txactions = $doc->getElementsByTagName('txaction');
    foreach($txactions as $txaction) {
      $item = array();
      $item['guid'] = self::firstTagValue($txaction,'guid');
      $item['date'] = strtotime(self::firstTagValue($txaction,'original-date'));
      $item['amount'] = self::firstTagValue($txaction,'amount');
  
      $displayname = strtolower(self::firstTagValue($txaction,'display-name'));
      $rawname = strtolower(self::firstTagValue($txaction,'raw-name'));
      if($displayname !== $rawname)
        $item['name'] = "$displayname ($rawname)";
      else
        $item['name'] = $rawname;
  
      $rawtxntype = strtolower(self::firstTagValue($txaction,'raw-txntype'));
      if($rawtxntype === 'dep')
        $item['txntype'] = 'deposit';
      else
        $item['txntype'] = $rawtxntype;
  
      $item['currency'] = $currency;
      $result_rows[] = $item;
    }
    return new FeedsParserResult(array('items' => $result_rows));
  }

  function firstTagValue($element,$tagToFind)
  {
    $tags = $element->getElementsByTagName($tagToFind);
    return $tags->item(0)->nodeValue;
  }


  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    return array(
      'guid' => array(
        'name' => t('Transaction GUID'),
        'description' => t('Global Unique Identifier of the transaction.'),
      ),
      'date' => array(
        'name' => t('Transaction Date'),
        'description' => t('Transaction date ("original-date").'),
      ),
      'amount' => array(
        'name' => t('Transaction Amount'),
        'description' => t('Transaction amount.'),
      ),
      'name' => array(
        'name' => t('Transaction Name'),
        'description' => t('Description of the transaction (combination of "display-name" and "raw-name", lowercase).'),
      ),
      'txntype' => array(
        'name' => t('Transaction Type'),
        'description' => t('Transaction type ("raw-txntype").'),
      ),
      'currency' => array(
        'name' => t('Transaction Currency'),
        'description' => t('Transaction currency (common across all transactions in the account feed).'),
      ),
     );
  }
}